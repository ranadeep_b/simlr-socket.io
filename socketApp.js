var express 	  = require('express'),
	mongoose 	  = require('./routes/models/dbConnection/dbConnection');
	http 		  = require('http'),
	_ 			  = require('lodash'),
	axon 	  	  = require('axon'),
	sio 		  = require('socket.io'),
	async 		  = require('async'),
	util 		  = require('util'),
	fs 			  = require('fs'),
	utils 		  = require('./node_modules/express/node_modules/connect/lib/utils.js'),
	cookieParser  = require('./node_modules/express/node_modules/cookie');

var app = express();

app.configure(function(){
	app.use(express.logger('dev'));
	app.use(express.static(__dirname+'/public'));
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack	  : true
	}));
})

var server = http.createServer(app),
	io	   = sio.listen(server);
server.listen(8081); //Socket IO server listens on 4700
console.log('Socket.IO listening on 8081');

/*-------------Domains and error handling ------------*/

var domain 		 = require('domain'),
	socketDomain = domain.create(),
	dbDomain 	 = domain.create();

socketDomain.on('error',function(err){
	util.log(err);
	fs.appendFile('socketError.txt',err.message+'\r'+err.stack,function(err){
		if(err) console.log('Error writing to socketError file ! lol')
	})
})

dbDomain.on('error',function(err){
	util.log(err);
	fs.appendFile('dbError.txt',err.message+'\r'+err.stack,function(err){
		if(err) console.log('Error writing to socketError file ! lol')
	})
})

/*-----------------------Mongoose Model ---------------*/
mongoose.connection.on('open',dbDomain.bind(function(){
	console.log('mongoose connected to db :D');
}))

mongoose.connection.on('close',dbDomain.bind(function(){
	util.log('Closed mongodb. Exiting process');
}));

mongoose.connection.on('error',dbDomain.bind(function(){
	console.log('Error connected to database..:(');
	//Send alert to me somehow
}));

//Sessions Schema copied from express-mongodb.js module for consistency
var sessionSchema = new mongoose.Schema({
    sid : {
        type : String,
        required : true,
        unique : true,
        index  : true
    },
    data : {},
    lastAccess : {
        type : Date,
        index : true
    },
    expires : {
        type : Date,
        index : true
    }
});

try{
	var models = {
			UserStatus  : require('./routes/models/UserStatus'),
		 	OnlineUsers : require('./routes/models/OnlineUsers'),
		 	Sessions 	: mongoose.model('session',sessionSchema),
		 	UserDetail  : require('./routes/models/UserDetails'),
		 	UserProfile : require('./routes/models/UserProfiles'),
		 	Chats 		: require('./routes/models/Chats'),
		 	Threads		: require('./routes/models/Threads')
	}

} catch(err){
	dbDomain.run(function(){
		throw new Error(err);
	})
}


/* -------------------------------------SOCKET.IO CONFIGURATION------------- -------------------------*/
//Socket IO Authorisation and Configuration
socketDomain.run(function(){

	io.configure(function(){
		io.set('authorization',function(handShakeData,cb){
			try{
				var cookie 	  = handShakeData.headers.cookie;
					signedSID = cookieParser.parse(cookie)['connect.sid'];
					sid 	  = utils.parseSignedCookie(signedSID,'shoutbox');

				models.Sessions.findOne({sid : sid},function(err,doc){
					if(err || !doc){
						cb(err,false);
						return;
					}
					else {
						if(!doc.expires || new Date < doc.expires && !(_.isUndefined(doc.data.userid)) ){
							handShakeData.userid = doc.data.userid;         // Storing userid in the socket , can be access in socket.handshake.userid
							cb(null,true);
							return;
						}
						else cb(null,false);
					}
				})

			} catch(err){
				cb(err,false)
			}


		})
	/*
		io.set('transports', [
	    	'websocket'
	  		, 'htmlfile'
	  		, 'xhr-polling'
	  		, 'jsonp-polling'
		]);
		io.enable('browser client minification');  // send minified client
		io.enable('browser client etag');          // apply etag caching logic based on version number
		io.enable('browser client gzip');          // gzip the file
		io.set('log level', 1);                    // reduce logging
	*/
	})
})
/*------------------------Extra Methods ---------------------*/
try{
	var addOnlineUser    = function(userId,socketId,cb){
		async.parallel([
			function(cbt){
				models.UserDetail.findOne({_id : userId},dbDomain.bind(function(err,doc){
					models.OnlineUsers.create({
						socketId 	: socketId,
						avlblStatus : true,
						name 		: doc.name,
						user_id 	: doc._id,
						fbId 		: doc.fbId,
						userPic 	: doc.normalPic,
						userp_id 	: doc.userp_id,
						params 		: doc.params,
						top5Params  : doc.top5Params,
						contacts 	: doc.contacts,
						gender 		: doc.gender,
						rank	 	: doc.rank,
						fbLikes 	: doc.fbLikes,
						fbFriends 	: doc.fbFriends,
						reqParams 	: doc.reqParams,
						top5Compliments : doc.top5Compliments
					},dbDomain.bind(function(err){
						if(err) cbt(err,null);
						else cbt(null,true)
					}))
				}))
			},
			function(cbt){
				models.UserStatus.update({user_id : userId},{status : true},cbt)
			}
			],
			cb
		);
	}

	var removeOnlineUser = function(userId,socketId,cb){
		async.parallel([
			function(cbt){
				models.OnlineUsers.remove({user_id : userId},cbt);
			},
			function(cbt){
				models.UserStatus.update({user_id : userId},{status : false,lastOnline : Date.now()},cbt);
			}
			],
			cb
		);
	}
} catch(err){
	dbDomain.run(function(){
		throw new Error(err);
	})
}

//store all the connected sockets with their socket.id in globalSocketsMap 
var globalSocketsMap = {},globalTimeoutsMap = {};

socketDomain.run(function(){

	io.sockets.on('connection',function(socket){

		try{
			var userId 		= socket.handshake.userid,
				mySocketId  = socket.id;
		} catch(err){
			socket.disconnect();
			return;
		}

		globalSocketsMap[mySocketId] = socket;

		socket.on('writingOn',function(obj){
			if(obj['threadId']){
				models.Threads.findOne({_id : obj.threadId},'participants',dbDomain.bind(function(err,doc){
					if(err || !doc) ;
					else {
						var twinId = doc.participants[0].equals(userId) ? doc.participants[1] : doc.participants[0];
						models.OnlineUsers.find({user_id : twinId,avlblStatus : true},'socketId',dbDomain.bind(function(err,docs){
							_.each(docs,function(doc){
								if(doc.socketId in globalSocketsMap) globalSocketsMap[doc.socketId].emit('writingOn');
							})
						}))
					}
				}));
			}
			else {
				models.Chats.findOne({_id : obj.chatId},'userSocketId twinSocketId',dbDomain.bind(function(err,doc){
					if(err || !doc) ;
					else {
						var twinSocketId = doc.userSocketId === socket.id ? doc.twinSocketId : doc.userSocketId;
						if(twinSocketId in globalSocketsMap) globalSocketsMap[twinSocketId].emit('writingOn');
						else socket.emit('closeChat');
					}
				}))
			}
		})

		socket.on('writingOff',function(obj){
			if(obj['threadId']){
				models.Threads.findOne({_id : obj.threadId},'participants',dbDomain.bind(function(err,doc){
					if(err || !doc) ;
					else {
						var twinId = doc.participants[0].equals(userId) ? doc.participants[1] : doc.participants[0];
						models.OnlineUsers.find({user_id : twinId,avlblStatus : true},'socketId',dbDomain.bind(function(err,docs){
							_.each(docs,function(doc){
								if(doc.socketId in globalSocketsMap) globalSocketsMap[doc.socketId].emit('writingOff');
							})
						}))
					}
				}))
			}
			else {
				models.Chats.findOne({_id : obj.chatId},'userSocketId twinSocketId',dbDomain.bind(function(err,doc){
					if(err || !doc) ;
					else {
						var twinSocketId = doc.userSocketId === socket.id ? doc.twinSocketId : doc.userSocketId;
						if(twinSocketId in globalSocketsMap) globalSocketsMap[twinSocketId].emit('writingOff');
						else socket.emit('closeChat');
					}
				}));
			}
		})

		addOnlineUser(userId,mySocketId,socketDomain.bind(function(err){
			if(err) ;
		}))

		socket.on('disconnect',function(){
			removeOnlineUser(userId,mySocketId,socketDomain.bind(function(err){
				if(err) ;
			}))

			if(socket.id in globalSocketsMap) delete globalSocketsMap[socket.id];

			models.Chats.findOneAndUpdate(
				{$or : [{userSocketId : mySocketId},{twinSocketId : mySocketId}],closed : false}
				,{closed : true}
				,dbDomain.bind(function(err,doc){
					if(err) {
						console.log(err);
						return;
					}
					if(doc){
						var twinSocketId = doc.userSocketId === mySocketId ? doc.twinSocketId : doc.userSocketId;
						globalSocketsMap[twinSocketId].emit('closeChat');
					}
				}
			));
		});
	});
//closing socketDomain run
})
/*----------------------- AXON Connection for app(s)-socket.io communication---------------*/
var axonDomain = domain.create();

axonDomain.on('error',function(err){
	util.log(err);
	fs.appendFile('axonErrors.txt',err,function(err){
		if(err) console.log('lol error writing to error file');
	})
})

axonDomain.run(function(){



	var respondToApp = axon.socket('rep');
		respondToApp.format('json');
		respondToApp.bind(4747);   
	    /* <------------------- THE RESPONDER SERVER, make sure port is closed for other ips */

	    /*-------------events ---------------*/
	    respondToApp.on('bind',function(){
	    	console.log('Axon server is bound on 4747');
	    })

	    respondToApp.on('connect',function(){
	    	util.log('Client connected to axon on 4747')
	    })

	respondToApp.on('message',function(type,options,cb){
		if(options.socketId in globalSocketsMap){
			var socket = globalSocketsMap[options.socketId];
			switch(type) {
				case 'on' 	:
					//Change to switch case if there are more
					if(options.eventName === 'twinAreYouReadyResponse'){
						if(options.twinSocketId in globalSocketsMap){
								var timeoutId = setTimeout(function(){
									if(timeoutId in globalTimeoutsMap){
										if(options.twinSocketId in globalSocketsMap) globalSocketsMap[options.twinSocketId].removeAllListeners('twinAreYouReadyResponse');
										socket.emit('AreYouReadyResponse',{response : false});
										clearTimeout(timeoutId);
										delete globalTimeoutsMap[timeoutId];
									}
								},15*1000);

								globalTimeoutsMap[timeoutId] = timeoutId;

							globalSocketsMap[options.twinSocketId].on('twinAreYouReadyResponse',function(obj){
								socket.emit('AreYouReadyResponse',obj);
								clearTimeout(timeoutId);
								delete globalTimeoutsMap[timeoutId];
								globalSocketsMap[options.twinSocketId].removeAllListeners('twinAreYouReadyResponse');
							})
						}
						else {
							socket.emit('AreYouReadyResponse',{response : false});
						}
						cb(true);
					} 
					break;
				case 'emit' :
					socket.emit(options.eventName,options.emitData);
					cb(true)
					break;
				case 'removeAllListeners' :
					socket.removeAllListeners(options.eventName);
					cb(true);
					break;
				default :
					console.log('Something wrong with axon messages. default called !');
					cb(false)
					break;
			}
		}
		else cb(false);
	})
//Closing axon domain
})

/*-------------------------Execute everytime server starts , Server on start cleanup------------------*/
models.OnlineUsers.remove({},dbDomain.bind(function(err){
		if(err) console.log(err);
}))
models.UserStatus.update({status : true},{status : false,lastOnline : Date.now()},{multi : true},dbDomain.bind(function(err){
		if(err) console.log(err);
}));

