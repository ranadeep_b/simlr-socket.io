var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var onlineUsers 	    = new Schema({
	socketId 	: {type : String, index : true},
	avlblStatus : {type: Boolean,default: true},
	name 		: String,
	user_id		: {type: ObjectId,index: true,ref: 'UserDetail'},
	fbId 		: Number,
	userPic 	: {type: String},
	userp_id    : {type: ObjectId,ref: 'UserProfile'},
	params		: [{type : Number,index: true}],
	top5Params  : [{type: Number,index : true}],
	contacts 	: [{user : ObjectId,started : Date,_id : false}],
	gender 		: Boolean,
	rank 		: Number,
	fbLikes 	: [Number],
	fbFriends   : [Number],
	reqParams 	: [{pid: Number, checked: {type : Number, default : 0},_id : false}],
	top5Compliments : [Number]
})

module.exports = mongoose.model('OnlineUsers',onlineUsers)
