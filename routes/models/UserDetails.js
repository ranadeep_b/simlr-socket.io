var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var notificationSchema = require('./schemas/notificationSchema.js');

var capitalize = function(val){
	var Name  = '';
	if(typeof val != 'string') return val='' ; // check 
	val.split(' ').forEach(function(v){
		Name += v.charAt(0).toUpperCase()+v.substring(1)+" " ;
	});
	return Name.trim();
}

var userDetailSchema	= new Schema({
	preRegId 		: ObjectId,
	userp_id		: {type: ObjectId, index: true},
	name 			: {type: String, set: capitalize, index: true},
	normalPic 		: {type: String,default : '/img/ppics/default.png'}, // change in the setMyPic route
	thumb 			: {type: String,default : '/img/thumbs/default.png'},
	params          : [{type : Number, index : true}],
	reqParams 		: [{pid: Number, checked: {type : Number, default : 0},_id : false}],
	top5Params 		: [Number],
	allCompliments 	: [{pid : Number,checked: {type : Number, default : 0},_id : false}],
	testimonies 	: [{givenBy : {type : ObjectId, ref : 'UserDetail'}, chatId : ObjectId, testimony : String,_id : false}],
	top5Compliments : [Number],
	gender 			: {type : Boolean, default : true},
	password		: String,
	tags 			: [{tag : { type : ObjectId, ref : 'Tag'}, subscribed_on : { type : Number } , _id : false}],
	contacts		: [{user : { type : ObjectId, ref : 'UserDetail'} , started : Date ,_id : false}],
	allThreads 		: [{threadId : {type : ObjectId , ref : 'Thread'},twinId : ObjectId,lastMessage : String,lastUpdated : Number,_id : false}],
	allNotifications: [notificationSchema],
	karma 			: {type: Number ,default : 50},
	reportCount 	: {type: Number, default : 0,min : 0},
	reportLog 		: [{who : {type : ObjectId , ref : 'UserDetail'}, timeStamp : {type : Number, default : Date.now}, chatId : {type : ObjectId, ref : 'OnlineUsers'}, _id : false}],
	clearSlate 		: {type : Boolean,default : false},
	rank 			: {type: Number,default : 3,min :1,max : 5},
	images 			: [String],
	fbId 			: {type : Number,index : true},
	fbAccessToken 	: String,
	fbExpires 		: String,
	fbLikes 		: [Number],
	fbFriends 		: [Number]
})

module.exports = mongoose.model('UserDetail',userDetailSchema);