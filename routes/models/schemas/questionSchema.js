var mongoose   = require('mongoose');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var questionSchema = new Schema({
		q 		   : String,
		pid 	   : {type : Number,index : true},
		randomNo   : {type : Number,index : true}
})

module.exports = questionSchema