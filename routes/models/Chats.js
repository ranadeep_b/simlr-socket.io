var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var messageSchema 	= require('./schemas/messageSchema'),
	questionSchema 	= require('./schemas/questionSchema')


var chatSchema 		= new Schema({
	type      		: Number, // 0 --> normal ; 1--> shoutRespond
	names 			: [String],
	userId 			: {type : ObjectId},
	userRating 		: Number,
	userSocketId 	: String,
	twinId 			: {type : ObjectId},
	twinRating 		: Number,
	userTestimony 	: String,
	twinTestimony	: String,
	twinSocketId 	: String,
	messages 		: [messageSchema],
	started 		: {type : Number, default : Date.now},
	ended 			: {type :Number,default : Date.now},
	revealRequests 	: [{type : ObjectId}],
	revealed 		: {type : Boolean, default : false},
	contactRequests : [{type : ObjectId}],
	closed			: Boolean,
	userReported 	: {type : Boolean, default : false},
	twinReported    : {type : Boolean, default : false},
	questionPool 	: [questionSchema],
	askedQuestions 	: [{type : ObjectId}],
	askedPssts 		: [{type : ObjectId}],
	currentQuestion : {type : ObjectId,default : null},
	currentResponseCount : {type : Number,default : 0},
	randomNo 		: Number,
	userAnswers 	: [{qid : {type : ObjectId,index : true},answer : String,_id : false}],
	twinAnswers 	: [{qid : {type : ObjectId,index : true},answer : String,_id : false}],
	shouterId 		: {type : ObjectId, default : null},
	shoutId 		: {type : ObjectId, default : null}
})

module.exports = mongoose.model('Chat',chatSchema);